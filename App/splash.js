/* eslint-disable eslint-comments/no-unused-disable */
/* eslint-disable prettier/prettier */
/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  TouchableHighlight,
  Image,
  Linking,
  Platform,
  Keyboard,

} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
// eslint-disable-next-line prettier/prettier
import AsyncStorage from '@react-native-community/async-storage';
/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


export class splash extends Component {


  constructor(props) {
    super(props);
    this.state = {
      textbox1: '',
      textbox2: '',
      spinner: false,
      wrongUP: '',

    };

  }

  componentDidMount() {
    setTimeout(() => {

      Actions.login({ type: ActionConst.RESET });
    }, 2000);


  }

  render() {
    return (

      <View style={styles.container} >
  <Image style={{marginTop:WINDOW_HEIGHT/7}} source={require('./assets/logo1_white.png')} />

  <Image style={{marginBottom:WINDOW_HEIGHT/7}} source={require('./assets/logo3.png')} />


      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1b75bb',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default splash;
