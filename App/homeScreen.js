/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-return-assign */
/* eslint-disable prettier/prettier */
/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    TouchableHighlight,
    Image,
    Linking,
    Platform,
    Keyboard,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { font_family } from './design';

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


export class HomeScreen extends Component {


    constructor(props) {
        super(props);
        this.state = {
            textbox1: '',
            textbox2: '',
            spinner: false,
            wrongUP: '',
            time: this.props.time

        };

    }

    componentDidMount() {
        if (this.state.time) {
            this.dropDownAlertRef.alertWithType('success', 'Result in Progress', 'Please wait for ' + this.state.time);
        }
    }


    /**
     * @loginFun :- This function is used to login user through api with credential validations.
     * @api :- 'https://mowbuddy-uat-api.azurewebsites.net/api/Account/login'
     */

    componentWillUnmount() {
        Keyboard.dismiss();
    }
    resultDetailsFun() {
        this.setState({ spinner: true })
        debugger
        fetch('https://st4.idsil.com/check_job_id/1',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },

            })

            .then((response) => response.json())
            .then((responseData) => {
                debugger;
                this.setState({ spinner: false })
                Actions.ResultScreen({ ResultJson: responseData.ranked_data })

                //this.dropDownAlertRef.alertWithType('success', 'Notification', 'Success');

            })
            .catch((error) => {
                this.dropDownAlertRef.alertWithType('error', 'Error', error);
                console.error(error);
            });
    }


    /**
     * @forgotpassFun :- This function consists of functionality of Forgot password.
     */

    forgotpassFun() {
        this.dropDownAlertRef.alertWithType('success', 'Comming soon', 'This functionality is under progress');
    }



    render() {
        return (

            <View style={styles.container} >


              


                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <Image style={{margin:40}} source={require('./assets/logo1.png')} />
                <TouchableOpacity onPress={() => {
                    Actions.FormScreen(); // this.loginFun();
                }}>
                    <View style={styles.buttonStyle}>
                        <Text style={[styles.textStyle3, { marginTop: 0 }]}>Search Candidate</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {

                    this.resultDetailsFun()
                }}>
                    <View style={styles.buttonStyle}>
                        <Text style={[styles.textStyle3, { marginTop: 0 }]}>View Results</Text>
                    </View>
                </TouchableOpacity>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
       // justifyContent: 'center',
    },

    textStyle3: {
        color: 'grey',
        fontSize: 22,
        marginTop: 13,
        fontWeight: 'bold',

    },
    buttonStyle: {
        borderWidth: 1.5,
        backgroundColor: '#fff',
        borderColor: '#1b75bb',
        height: WINDOW_HEIGHT / 13,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
        width: WINDOW_WIDTH / 1.08,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 25,
    },
});

export default HomeScreen;
