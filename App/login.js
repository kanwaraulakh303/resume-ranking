/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  TouchableHighlight,
  Image,
  Linking,
  Platform,
  Keyboard,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
//import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { font_family } from './design';

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


export class login extends Component {


  constructor(props) {
    super(props);
    this.state = {
      textbox1: 'test@test.com',
      textbox2: 'test@123',
      spinner: false,
      wrongUP: '',

    };

  }

  componentWillMount() {

  }


  /**
   * @loginFun :- This function is used to login user through api with credential validations.
   * @api :- 'https://mowbuddy-uat-api.azurewebsites.net/api/Account/login'
   */

  componentWillUnmount() {
    Keyboard.dismiss();
  }


loginFun()
{
  if(this.state.textbox1 === "test@test.com" && this.state.textbox2=== "test@123")
  {
    Actions.HomeScreen();
  }
else{
  this.setState({textbox1:'',textbox2:''})
  this.dropDownAlertRef.alertWithType('error', 'Wrong entry', 'Please enter correct Username/Password ');
}
}
  /**
   * @forgotpassFun :- This function consists of functionality of Forgot password.
   */

  forgotpassFun() {
    this.dropDownAlertRef.alertWithType('success', 'Comming soon', 'This functionality is under progress');
  }



  render() {
    return (

      <View style={styles.container} >


       


        {/* <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        /> */}
        <View style={styles.position}>
          <Image source={require('./assets/logo1_white.png')} />
          {/* <Text style={{color:"#fff",fontWeight:'bold',fontSize:35}}>RESUME RANKING</Text>  */}
        </View>
        <View style={{}} >
          <Text style={styles.textStyle3}>Username</Text>
          <TextInput
            style={styles.textboxDesign}
            onChangeText={(textbox1) => this.setState({ textbox1, wrongUP: '' })}
            value={this.state.textbox1}
          />
          <Text style={styles.textStyle3}>Password</Text>
          <TextInput
            style={styles.textboxDesign}
            secureTextEntry={true}
            onChangeText={(textbox2) => this.setState({ textbox2, wrongUP: '' })}
            value={this.state.textbox2}
          />

          <View style={{ margin: 5, alignItems: 'center' }}>
            <Text style={{ color: 'red', fontSize: 16 }}>{this.state.wrongUP}</Text>
          </View>

          <TouchableOpacity onPress={() => {
           
             this.loginFun();
          }}>
            <View style={{  borderWidth: 1.5, borderColor: '#fff', height: WINDOW_HEIGHT / 13, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[styles.textStyle3, { marginTop: 0 }]}>Login</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => {
              this.forgotpassFun();
            }}>
              <View style={{ marginTop: WINDOW_HEIGHT / 30, alignItems: 'center', margin: 10 }}>
                <Text style={{ color: '#fff', fontSize: 17, textDecorationLine: 'underline' }}>Forgot your password?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
              Actions.SignUp();
            }}>
              <View style={{ marginTop: WINDOW_HEIGHT / 30, alignItems: 'center', margin: 10 }}>
                <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold', textDecorationLine: 'underline' }}>Sign Up</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: '#1b75bb',
    paddingBottom: WINDOW_HEIGHT / 8,
    justifyContent: 'center',

  },
  position: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 20,
    
  },
  textStyle: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fff',

  },
  textStyle2: {
    fontSize: 20,
    color: '#fff',

  },
  textboxDesign: {
    height: WINDOW_HEIGHT / 13,
    backgroundColor: '#fff',
    borderRadius: 3.5,
    fontSize: 20,
    marginTop: 5,
    color: 'grey',
    paddingLeft: 10,
    shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        borderColor:'grey',
        borderWidth:.5
  },
  textStyle3: {
    color: '#fff',
    fontSize: 18,
    marginTop: 13,

  },
  spinnerTextStyle: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 25,
  },
  backgroundImage: {

  },






  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },

});

export default login;
