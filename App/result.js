/* eslint-disable semi */
/* eslint-disable no-debugger */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-return-assign */
/* eslint-disable prettier/prettier */
/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    TouchableHighlight,
    Image,
    Linking,
    Platform,
    Keyboard,
    FlatList,
    ScrollView,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
//import Spinner from 'react-native-loading-spinner-overlay';
//import { ResultJson } from './constants/index';
import AsyncStorage from '@react-native-community/async-storage';
import { font_family } from './design';
//import { ScrollView } from 'react-native-gesture-handler';

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


export class ResultScreen extends Component {


    constructor(props) {
        super(props);
        this.state = {
            textbox1: '',
            textbox2: '',
            spinner: false,
            wrongUP: '',
            ResultJson:this.props.ResultJson

        };

    }

    componentWillMount() {
       
    }


    /**
     * @loginFun :- This function is used to login user through api with credential validations.
     * @api :- 'https://mowbuddy-uat-api.azurewebsites.net/api/Account/login'
     */

    componentWillUnmount() {
        Keyboard.dismiss();
    }



    /**
     * @forgotpassFun :- This function consists of functionality of Forgot password.
     */

    forgotpassFun() {
        this.dropDownAlertRef.alertWithType('success', 'Comming soon', 'This functionality is under progress');
    }



    render() {
        return (
            <View>
            <ScrollView style={{ backgroundColor: '#fff' }}>
                <View style={styles.container} >


                  


                    {/* <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        /> */}
                    <View style={styles.headerStyle}>
                        <Text style={styles.headerTextStyle}>Required Skills</Text>
                        <FlatList
                            style={{ paddingTop: 10 }}
                            horizontal={true}
                            data={this.state.ResultJson.rqd_skills}
                            renderItem={({ item, index }) =>
                                <View style={{ marginLeft: 10 }} >

                                    <Text style={{ color: 'grey', fontSize: 15 }}>{item}</Text>

                                </View>


                            }
                            keyExtractor={item => item.id}
                        />
                    </View>
                    <View style={[styles.headerStyle, { marginTop: 20 }]}>
                        <Text style={styles.headerTextStyle}>Optional Skills</Text>
                        <FlatList
                            style={{ paddingTop: 10 }}
                            horizontal={true}
                            data={this.state.ResultJson.opt_skills}
                            renderItem={({ item, index }) =>
                                <View style={{ marginLeft: 10 }} >

                                    <Text style={{ color: 'grey', fontSize: 15 }}>{item}</Text>

                                </View>


                            }
                            keyExtractor={item => item.id}
                        />
                    </View>

                    <View style={{
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 1 },
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        elevation: 10, borderColor: 'grey', marginTop: 40, width: WINDOW_WIDTH, flexDirection: 'row', justifyContent: 'space-between'
                    }}>
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={styles.recordDesignView}>
                                        <Text style={[styles.recordDesignText, { width: WINDOW_WIDTH / 2 }]}>
                                            Name
                            </Text>
                                    </View>
                                    <View style={styles.recordDesignView}>
                                        <Text style={[styles.recordDesignText, { width: WINDOW_WIDTH / 6 }]}>
                                            Rank
                            </Text>
                                    </View>
                                    <View style={styles.recordDesignView}>
                                        <Text style={[styles.recordDesignText, { width: WINDOW_WIDTH / 3.5 }]}>
                                            Experience
                            </Text>
                                    </View>
                                    <View style={styles.recordDesignView}>
                                        <Text style={[styles.recordDesignText, { width: WINDOW_WIDTH / 3 }]}>
                                            Req.
                            </Text>
                                    </View>
                                    <View style={styles.recordDesignView}>
                                        <Text style={[styles.recordDesignText, { width: WINDOW_WIDTH / 3 }]}>
                                            Opt.
                            </Text>
                                    </View>
                                    <View style={styles.recordDesignView}>
                                        <Text style={[styles.recordDesignText, { width: WINDOW_WIDTH / 4 }]}>
                                            Report
                            </Text>
                                    </View>

                                </View>
                                <View>
                                    <FlatList
                                        // style={{ borderWidth: 3, borderColor: '#fff' }}
                                        data={this.state.ResultJson.result}
                                        renderItem={({ item, index }) =>
                                            <View>
                                                {index % 2 ?
                                                    <View style={{ flexDirection: 'row' }}>

                                                        <View style={[styles.recordDesignView2, { backgroundColor: 'lightgrey' }]}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 2 }]}>
                                                                {item.name}
                                                            </Text>
                                                        </View>
                                                        <View style={[styles.recordDesignView2, { backgroundColor: 'lightgrey' }]}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 6 }]}>
                                                                {index + 1}
                                                            </Text>
                                                        </View>
                                                        <View style={[styles.recordDesignView2, { backgroundColor: 'lightgrey' }]}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 3.5 }]}>
                                                                {item.overall_years_experience}
                                                            </Text>
                                                        </View>
                                                        <View style={[styles.recordDesignView2, { backgroundColor: 'lightgrey' }]}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 3 }]}>
                                                                {this.state.ResultJson.rqd_skills[0]}
                                                            </Text>
                                                        </View>
                                                        <View style={[styles.recordDesignView2, { backgroundColor: 'lightgrey' }]}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 3 }]}>
                                                                {this.state.ResultJson.opt_skills[0]}
                                                            </Text>
                                                        </View>
                                                        <View style={[styles.recordDesignView2, { backgroundColor: 'lightgrey' }]}>
                                                            <TouchableOpacity onPress={() => {

                                                                Actions.ResultDetails({ details: item, rank: index + 1 });
                                                                debugger
                                                            }}>
                                                                <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 4,fontWeight:'bold', color: '#1b75bb', fontWeight:'bold', color: '#1b75bb', textDecorationLine: 'underline', }]}>
                                                                    View

                                    </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                    :
                                                    <View style={{ flexDirection: 'row' }}>

                                                        <View style={styles.recordDesignView2}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 2 }]}>
                                                                {item.name}
                                                            </Text>
                                                        </View>
                                                        <View style={styles.recordDesignView2}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 6 }]}>
                                                                {index + 1}
                                                            </Text>
                                                        </View>
                                                        <View style={styles.recordDesignView2}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 3.5 }]}>
                                                                {item.overall_years_experience}
                                                            </Text>
                                                        </View>
                                                        <View style={styles.recordDesignView2}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 3 }]}>
                                                                {this.state.ResultJson.rqd_skills[0]}
                                                            </Text>
                                                        </View>
                                                        <View style={styles.recordDesignView2}>
                                                            <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 3 }]}>
                                                                {this.state.ResultJson.opt_skills[0]}
                                                            </Text>
                                                        </View>
                                                        <View style={styles.recordDesignView2}>
                                                            <TouchableOpacity onPress={() => {
                                                                Actions.ResultDetails({ details: item, rank: index + 1 });
                                                                debugger
                                                            }}>
                                                                <Text style={[styles.recordDesignText2, { width: WINDOW_WIDTH / 4, color: 'blue', fontWeight:'bold', color: '#1b75bb', textDecorationLine: 'underline' }]}>
                                                                    View

                                    </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>}
                                            </View>
                                        }
                                        keyExtractor={item => item.id}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ width: WINDOW_WIDTH, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#1b75bb', margin: 5, fontSize: 10 }}>
                            Scroll Left-Right for more
        </Text>
                    </View>
                </View>

            </ScrollView>
              <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
              </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
        alignItems: 'center',

    },

    headerStyle: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 10,
        borderColor: 'grey',
        width: WINDOW_WIDTH / 1.08,
        height: WINDOW_HEIGHT / 7,
        backgroundColor: '#fff',
        padding: 5,
        borderRadius: 5,
    },
    headerTextStyle: {
        marginLeft: 10,
        color: '#1b75bb',
        fontSize: 20,
        fontWeight: 'bold',
    },
    recordDesignText: {
        fontSize: 17,
        fontWeight: 'bold',
        margin: 5,
        color: '#fff',
    },
    recordDesignText2: {
        fontSize: 15,
        margin: 5,
        color: 'grey',
    },
    recordDesignView: {
        backgroundColor: '#1b75bb',
        // height: WINDOW_HEIGHT / 20,
        justifyContent: 'center',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderColor: 'darkgrey',

    },
    recordDesignView2: {
        backgroundColor: '#fff',
        // height: WINDOW_HEIGHT / 20,
        justifyContent: 'center',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderColor: 'darkgrey',

    },

});

export default ResultScreen;
