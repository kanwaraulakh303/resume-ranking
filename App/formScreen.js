/* eslint-disable eqeqeq */
/* eslint-disable no-dupe-keys */
/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { Component, Fragment } from 'react';
import SearchableDropdown from 'react-native-searchable-dropdown';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    TouchableHighlight,
    Image,
    Linking,
    Platform,
    Keyboard,
    FlatList,
    ScrollView,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
import DropdownAlert from 'react-native-dropdownalert';
import { debug } from 'react-native-reanimated';
var items = [
    {
        id: 1,
        name: 'JavaScript',
    },
    {
        id: 2,
        name: 'Java',
    },
    {
        id: 3,
        name: 'Ruby',
    },
    {
        id: 4,
        name: 'React Native',
    },
    {
        id: 5,
        name: 'PHP',
    },
    {
        id: 6,
        name: 'Python',
    },
    {
        id: 7,
        name: 'Go',
    },
    {
        id: 8,
        name: 'Swift',
    },
];

var items2 = [
    {
        id: 1,
        name: 'Angular',
    },
    {
        id: 2,
        name: 'Node',
    },
    {
        id: 3,
        name: 'Express',
    },
    {
        id: 4,
        name: 'Flutter',
    },
    {
        id: 5,
        name: 'Golang',
    },

];
var items3 = [
    {
        id: 1,
        name: 'Banglore',
    },
    {
        id: 2,
        name: 'Mohali',
    },
    {
        id: 3,
        name: 'Chandigarh',
    },
    {
        id: 4,
        name: 'Hyderabad',
    },
    {
        id: 5,
        name: 'Gurugram',
    },

];
/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class FormScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItems: [],
            selectedItems2: [],
            createdTag: '',
            createdTag2: '',
            createdTag3: '',
            searchResult: false,
            textbox1: '',
            textbox2: '',
            textbox3: '',
            spinner: false,
        };
    }

    searchFun() {
        if (this.state.selectedItems.length < 1) {
            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Please enter Required Skills');
        }
        else if (this.state.selectedItems2.length < 1) {
            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Please enter Optional Skills');
        }
        else if (this.state.textbox1.length < 1) {
            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Please enter Min Exp');
        }
        else if (this.state.textbox2.length < 1) {
            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Please enter Max Exp');
        }
        else if (this.state.createdTag3.length < 1) {
            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Please enter Location');
        }
        else if (this.state.textbox3.length < 1) {
            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Please enter No. of Resume');
        }
        else {
            let RSkills = this.state.selectedItems[0].name;
            let OSkills = this.state.selectedItems2[0].name;
            for (i = 0; i < this.state.selectedItems.length; i++) {
                if (i) {
                    RSkills = RSkills + ',' + this.state.selectedItems[i].name;
                }
            }
            for (i = 0; i < this.state.selectedItems2.length; i++) {
                if (i) {
                    OSkills = OSkills + ',' + this.state.selectedItems2[i].name;
                }
            }
            debugger;

            this.setState({ spinner: true });
            fetch('https://st4.idsil.com/create_job_spec',
              {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                 body: JSON.stringify({

                'req_skills':RSkills,
                'optional_skills':OSkills,
                'location':this.state.createdTag3,
                'min_exp':this.state.textbox1,
                'max_exp':this.state.textbox2,
                'nos_resumes_fetch':this.state.textbox3,
                //'job_id':13434535,
            }),
              })

              .then((response) => response.json())
              .then((responseData) => {
                debugger;

               this.setState({  spinner:false })
           
             Actions.HomeScreen({ type: ActionConst.RESET ,time:"5 min"});
              })
              .catch((error) => {
                this.dropDownAlertRef.alertWithType('error', 'Error', error);
                console.error(error);
              });
          }

        }

    render() {
        return (
<View>
            <ScrollView style={{  backgroundColor: '#fff'}} keyboardShouldPersistTaps="handled" >
                <View style={styles.container}>
                
                <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        /> 
                    <View style={{ margin: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'grey' }}>Technical Skills</Text>
                    </View>
                    <View>
                        <View>
                            <Text style={styles.heading}>Required Skills</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Fragment >

                                <SearchableDropdown

                                    onItemSelect={(item) => {
                                        let searchResult = false;
                                        for (i = 0; i < this.state.selectedItems.length; i++) {
                                            if (item.name == this.state.selectedItems[i].name) {
                                                searchResult = true;
                                            }

                                        }
                                        if (searchResult) {
                                            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Skills already exists');

                                        }
                                        else {

                                            const items = this.state.selectedItems;
                                            items.push(item);
                                            this.setState({ selectedItems: items });

                                        }

                                    }}
                                    containerStyle={{ padding: 5 }}

                                    itemStyle={{
                                        padding: 10,
                                        marginTop: 2,
                                        backgroundColor: '#1b75bb',
                                        borderColor: 'grey',
                                        borderWidth: 1,
                                        borderRadius: 5,
                                    }}
                                    itemTextStyle={{ color: '#fff' }}
                                    itemsContainerStyle={{ maxHeight: 140 }}
                                    items={items}
                                    // defaultIndex={2}
                                    resetValue={false}
                                    textInputProps={
                                        {

                                            //  placeholder: "placeholder",
                                            underlineColorAndroid: 'transparent',
                                            style: {
                                                padding: 12,
                                                borderWidth: .5,
                                                borderColor: 'grey',
                                                borderRadius: 5,
                                                fontSize: 15,
                                                backgroundColor: '#fff',
                                                width: WINDOW_WIDTH / 1.3,
                                                height: WINDOW_HEIGHT / 15,
                                                color:'grey',
                                                shadowColor: '#000',
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowOpacity: 0.8,
                                                shadowRadius: 2,
                                                elevation: 5,
                                            },
                                            onTextChange: text => this.setState({ createdTag: text }),
                                        }
                                    }
                                    listProps={
                                        {
                                            nestedScrollEnabled: true,
                                        }
                                    }
                                />

                            </Fragment>
                            {this.state.createdTag.length ?
                                <TouchableOpacity style={styles.button} onPress={() => {
                                    //  this.forgotpassFun();
                                    let searchResult = false;
                                    var item = { id: items.length + 1, name: this.state.createdTag };
                                    for (i = 0; i < this.state.selectedItems.length; i++) {
                                        if (item.name == this.state.selectedItems[i].name) {

                                            searchResult = true;
                                        }

                                    }

                                    if (searchResult) {

                                        this.dropDownAlertRef.alertWithType('error', 'Warning', 'Skills already exists');
                                    }

                                    else {

                                        const itemss = this.state.selectedItems;
                                        itemss.push(item);
                                        this.setState({ selectedItems: itemss });
                                    }

                                }}>

                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#fff' }}>Add</Text>

                                </TouchableOpacity>
                                :

                                <View style={[styles.button, { opacity: 0.5 }]}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#fff' }}>Add</Text>
                                </View>
                            }
                        </View>
                        <View  >
                            <FlatList

                                horizontal={true}
                                data={this.state.selectedItems}
                                renderItem={({ item, index }) =>
                                    <View style={{
                                        width: (item.name.length * 8) + 60,
                                        justifyContent: 'center',
                                        flex: 0,
                                        backgroundColor: '#eee',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        margin: 5,
                                        padding: 8,
                                        borderRadius: 15,
                                    }}>
                                        <Text style={{ color: '#555' }}>{item.name}</Text>
                                        <TouchableOpacity onPress={() => {
                                            this.state.selectedItems.splice(index, 1);
                                            this.setState({ abc: '' });
                                        }} style={{ backgroundColor: '#f16d6b', alignItems: 'center', justifyContent: 'center', width: 25, height: 25, borderRadius: 100, marginLeft: 10 }}>
                                            <Text>X</Text>
                                        </TouchableOpacity>
                                    </View>


                                }
                                keyExtractor={item => item.id}
                            />
                        </View>
                    </View>

                    <View>
                        <View>
                            <Text style={styles.heading}>Optional Skills</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Fragment >

                                <SearchableDropdown

                                    onItemSelect={(item) => {
                                        let searchResult = false;
                                        for (i = 0; i < this.state.selectedItems2.length; i++) {
                                            if (item.name == this.state.selectedItems2[i].name) {
                                                searchResult = true;
                                            }

                                        }
                                        if (searchResult) {
                                            this.dropDownAlertRef.alertWithType('error', 'Warning', 'Skills already exists');

                                        }
                                        else {

                                            const items2 = this.state.selectedItems2;
                                            items2.push(item);
                                            this.setState({ selectedItems2: items2 });

                                        }

                                    }}
                                    containerStyle={{ padding: 5 }}

                                    itemStyle={{
                                        padding: 10,
                                        marginTop: 2,
                                        backgroundColor: '#1b75bb',
                                        borderColor: 'grey',
                                        borderWidth: 1,
                                        borderRadius: 5,
                                    }}
                                    itemTextStyle={{ color: '#fff' }}
                                    itemsContainerStyle={{ maxHeight: 140 }}
                                    items={items2}
                                    // defaultIndex={2}
                                    resetValue={false}
                                    textInputProps={
                                        {

                                            //  placeholder: "placeholder",
                                            underlineColorAndroid: 'transparent',
                                            style: {
                                                padding: 12,
                                                borderWidth: .5,
                                                borderColor: 'grey',
                                                borderRadius: 5,
                                                fontSize: 15,
                                                backgroundColor: '#fff',
                                                width: WINDOW_WIDTH / 1.3,
                                                height: WINDOW_HEIGHT / 15,
                                                color:'grey',
                                                shadowColor: '#000',
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowOpacity: 0.8,
                                                shadowRadius: 2,
                                                elevation: 5,
                                            },
                                            onTextChange: text => this.setState({ createdTag2: text }),
                                        }
                                    }
                                    listProps={
                                        {
                                            nestedScrollEnabled: true,
                                        }
                                    }
                                />

                            </Fragment>
                            {this.state.createdTag2.length ?
                                <TouchableOpacity style={styles.button} onPress={() => {
                                    //  this.forgotpassFun();
                                    let searchResult = false;
                                    var item = { id: items2.length + 1, name: this.state.createdTag2 };
                                    for (i = 0; i < this.state.selectedItems2.length; i++) {
                                        if (item.name == this.state.selectedItems2[i].name) {

                                            searchResult = true;
                                        }

                                    }

                                    if (searchResult) {

                                        this.dropDownAlertRef.alertWithType('error', 'Warning', 'Skills already exists');
                                    }

                                    else {

                                        const itemss = this.state.selectedItems2;
                                        itemss.push(item);
                                        this.setState({ selectedItems2: itemss });
                                    }

                                }}>

                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#fff' }}>Add</Text>

                                </TouchableOpacity>
                                :

                                <View style={[styles.button, { opacity: 0.5 }]}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#fff' }}>Add</Text>
                                </View>
                            }
                        </View>
                        <View  >
                            <FlatList

                                horizontal={true}
                                data={this.state.selectedItems2}
                                renderItem={({ item, index }) =>
                                    <View style={{
                                        width: (item.name.length * 8) + 60,
                                        justifyContent: 'center',
                                        flex: 0,
                                        backgroundColor: '#eee',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        margin: 5,
                                        padding: 8,
                                        borderRadius: 15,
                                    }}>
                                        <Text style={{ color: '#555' }}>{item.name}</Text>
                                        <TouchableOpacity onPress={() => {
                                            this.state.selectedItems2.splice(index, 1);
                                            this.setState({ abc: '' });
                                        }} style={{ backgroundColor: '#f16d6b', alignItems: 'center', justifyContent: 'center', width: 25, height: 25, borderRadius: 100, marginLeft: 10 }}>
                                            <Text>X</Text>
                                        </TouchableOpacity>
                                    </View>


                                }
                                keyExtractor={item => item.id}
                            />
                        </View>
                    </View>
                    <View>
                        <View style={{ margin: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'grey' }}>Experience </Text>
                        </View>

                        <View>
                            <Text style={styles.heading}>Min Exp.</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.textboxDesign}

                                onChangeText={(textbox1) => this.setState({ textbox1 })}
                                value={this.state.textbox1}
                            />
                        </View>

                        <View style={{ marginTop: 8 }}>
                            <Text style={styles.heading}>Max Exp.</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.textboxDesign}

                                onChangeText={(textbox2) => this.setState({ textbox2 })}
                                value={this.state.textbox2}
                            />
                        </View>
                    </View>

                    <View>
                        <View style={{ margin: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'grey' }}>Location </Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Fragment >
                                <SearchableDropdown
                                    onItemSelect={(item) => {

                                        this.setState({ createdTag3: item.name });
                                    }}
                                    containerStyle={{ padding: 5 }}
                                    onRemoveItem={(item, index) => {
                                        const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                        this.setState({ selectedItems: items });
                                    }}
                                    itemStyle={{
                                        padding: 10,
                                        marginTop: 2,
                                        backgroundColor: '#1b75bb',
                                        borderColor: 'grey',
                                        borderWidth: 1,
                                        borderRadius: 5,
                                    }}
                                    itemTextStyle={{ color: '#fff' }}
                                    itemsContainerStyle={{ maxHeight: 140 }}
                                    items={items3}
                                    //   defaultIndex={2}
                                    resetValue={false}
                                    textInputProps={
                                        {

                                            //  placeholder: "placeholder",
                                            underlineColorAndroid: 'transparent',
                                            style: {
                                                padding: 12,
                                                borderWidth: 0.5,
                                                borderColor: 'grey',
                                                borderRadius: 5,
                                                fontSize: 15,
                                                backgroundColor: '#fff',
                                                width: WINDOW_WIDTH / 1.08,
                                                height: WINDOW_HEIGHT / 15,
                                                color:'grey',
                                                shadowColor: '#000',
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowOpacity: 0.8,
                                                shadowRadius: 2,
                                                elevation: 5,
                                            },
                                            onTextChange: text => this.setState({ createdTag3: text }),
                                        }
                                    }
                                    listProps={
                                        {
                                            nestedScrollEnabled: true,
                                        }
                                    }
                                />

                            </Fragment>
                        </View>
                    </View>

                    <View>
                        <View style={{ margin: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'grey' }}>No. of Resume</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TextInput
                                keyboardType="numeric"
                                style={styles.textboxDesign}

                                onChangeText={(textbox3) => this.setState({ textbox3 })}
                                value={this.state.textbox3}
                            />
                        </View>

                    </View>
                    <TouchableOpacity onPress={() => {

                        this.searchFun();
                    }}>
                        <View style={{ shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5, backgroundColor: '#1b75bb',  height: WINDOW_HEIGHT / 13, alignItems: 'center', justifyContent: 'center', width: WINDOW_WIDTH / 1.08, marginLeft: 15, marginTop: 45 }}>
                            <Text style={{ color: '#fff', fontSize: 20, marginTop: 13, marginTop: 0, fontWeight: 'bold', }}>Search</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>

<DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#fff',
        paddingBottom: WINDOW_HEIGHT / 8,
        // justifyContent: 'center',
    },
    heading: {
        marginLeft: 15,
        fontSize: 15,
        color: 'grey',
    },
    button: {
        backgroundColor: '#1b75bb',
        width: WINDOW_WIDTH / 6,
        height: WINDOW_HEIGHT / 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 6,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    textboxDesign: {
        height: WINDOW_HEIGHT / 15,
        backgroundColor: '#fff',
        borderRadius: 3.5,
        fontSize: 15,
        marginTop: 5,
        color: 'grey',
        borderColor:'grey',
        borderWidth:.5,
        width: WINDOW_WIDTH / 1.08,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 25,
      },

});
export default FormScreen;
