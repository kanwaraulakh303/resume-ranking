/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-return-assign */
/* eslint-disable prettier/prettier */
/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    TouchableHighlight,
    Image,
    Linking,
    Platform,
    Keyboard,
    FlatList,
    ScrollView
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
//import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { font_family } from './design';

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


export class ResultDetails extends Component {


    constructor(props) {
        super(props);
        this.state = {
            textbox1: '',
            textbox2: '',
            spinner: false,
            wrongUP: '',
            resultDetails: this.props.details,
            rank: this.props.rank

        };

    }

    componentWillMount() {

    }


    /**
     * @loginFun :- This function is used to login user through api with credential validations.
     * @api :- 'https://mowbuddy-uat-api.azurewebsites.net/api/Account/login'
     */

    componentWillUnmount() {
        Keyboard.dismiss();
    }



    /**
     * @forgotpassFun :- This function consists of functionality of Forgot password.
     */

    forgotpassFun() {
        this.dropDownAlertRef.alertWithType('success', 'Comming soon', 'This functionality is under progress');
    }



    render() {
        return (
            <View> 
            <ScrollView style={{ backgroundColor: '#fff' }}>
                <View style={styles.container} >


                   
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: WINDOW_WIDTH, padding: 10, flexWrap: 'wrap', borderBottomWidth: 1, borderColor: 'grey' }}>
                        <Text style={styles.textDesign}>{this.state.resultDetails.name}</Text>
                        <Text style={styles.textDesign}> / </Text>
                        <Text style={styles.textDesign}>Rank {this.state.rank}</Text>
                        <Text style={styles.textDesign}> / </Text>
                        <Text style={styles.textDesign}>Exp. {this.state.resultDetails.overall_years_experience}</Text>

                    </View>
                    <View style={{ maxHeight: WINDOW_HEIGHT / 2.3, }}>

                        <Text style={styles.headerDesign}>Academic</Text>

                        <FlatList

                            horizontal={true}
                            data={this.state.resultDetails.academic_summary_complete}
                            renderItem={({ item, index }) =>
                                <View style={styles.academicContainerDesign} >

                                    <Text style={styles.textDesign2}>--> {item.degree_level}</Text>
                                    <Text style={styles.textDesign2}>--> {item.degree}</Text>
                                    <Text style={styles.textDesign2}>--> {item.institute}</Text>
                                    <Text style={styles.textDesign2}>--> {item.year}</Text>

                                </View>


                            }
                            keyExtractor={item => item.id}
                        />

                        <View style={{ width: WINDOW_WIDTH, alignItems: 'flex-end' }}>
                            <Text style={{ color: '#1b75bb', margin: 5, fontSize: 10 }}>
                                Scroll Left-Right for more
        </Text>
                        </View>
                    </View>
                    <Text style={[styles.headerDesign,]}>Professional Skills</Text>
                    <View style={{ alignItems: 'center', margin: 10 }}>
                        <View style={[styles.cardShadow,{marginTop:0}]}>

                            <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', margin: 10, }}>Required Skills</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 5 }}>
                                <View style={{ opacity: 0, width: WINDOW_WIDTH / 2.5, alignItems: 'center' }}>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        Overall
                                </Text>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        100%
                                </Text>
                                </View>
                                <View style={{ width: WINDOW_WIDTH / 5, alignItems: 'center' }}>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        Overall
                                </Text>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        100%
                                </Text>
                                </View>

                                <View style={{ width: WINDOW_WIDTH / 3, alignItems: 'center' }}>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        Professional
                                </Text>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        100%
                                </Text>
                                </View>



                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 5 }}>
                                <FlatList
                                    data={this.state.resultDetails.technical_skills_metrics_overall_resume.ext_required_skills_unique}
                                    renderItem={({ item, index }) =>
                                        <View style={{ width: WINDOW_WIDTH / 2.5, margin: 5 }}>

                                            <Text style={{ color: 'grey' }}>
                                                {item}
                                            </Text>

                                        </View>


                                    }
                                    keyExtractor={item => item.id}
                                />


                                <FlatList
                                    data={this.state.resultDetails.technical_skills_metrics_overall_resume.ext_required_skills_unique}
                                    renderItem={({ item, index }) =>

                                        <View style={{ width: WINDOW_WIDTH / 5, alignItems: 'center', margin: 5 }}>
                                            <Text style={{ color: 'grey' }}>
                                                {this.state.resultDetails.technical_skills_metrics_overall_resume.ext_required_skills_mapped_frequency[item]}
                                            </Text>


                                        </View>

                                    }
                                    keyExtractor={item => item.id}
                                />



                                <FlatList
                                    data={this.state.resultDetails.technical_skills_metrics_overall_resume.ext_required_skills_unique}
                                    renderItem={({ item, index }) =>

                                        <View style={{ width: WINDOW_WIDTH / 3, alignItems: 'center', margin: 5 }}>
                                            <Text style={{ color: 'grey' }}>
                                                {this.state.resultDetails.technical_skills_metrics_overall_resume.ext_required_skills_mapped_frequency[item]}
                                            </Text>


                                        </View>

                                    }
                                    keyExtractor={item => item.id}
                                />



                            </View>

                        </View>

                        <View style={styles.cardShadow}>

                            <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', margin: 10, }}>Optional Skills</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 5 }}>
                                <View style={{ opacity: 0, width: WINDOW_WIDTH / 2.5, alignItems: 'center' }}>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        Overall
                                </Text>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        50%
                                </Text>
                                </View>
                                <View style={{ width: WINDOW_WIDTH / 5, alignItems: 'center' }}>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        Overall
                                </Text>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        50%
                                </Text>
                                </View>

                                <View style={{ width: WINDOW_WIDTH / 3, alignItems: 'center' }}>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        Professional
                                </Text>
                                    <Text style={[styles.textDesign2, { margin: 0 }]}>
                                        50%
                                </Text>
                                </View>



                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 5 }}>
                                <FlatList
                                    data={this.state.resultDetails.technical_skills_metrics_overall_resume.ext_optional_skills_unique}
                                    renderItem={({ item, index }) =>
                                        <View style={{ width: WINDOW_WIDTH / 2.5, margin: 5 }}>

                                            <Text style={{ color: 'grey' }}>
                                                {item}
                                            </Text>

                                        </View>


                                    }
                                    keyExtractor={item => item.id}
                                />


                                <FlatList
                                    data={this.state.resultDetails.technical_skills_metrics_overall_resume.ext_optional_skills_unique}
                                    renderItem={({ item, index }) =>

                                        <View style={{ width: WINDOW_WIDTH / 5, alignItems: 'center', margin: 5 }}>
                                            <Text style={{ color: 'grey' }}>
                                                {this.state.resultDetails.technical_skills_metrics_overall_resume.ext_optional_skills_mapped_frequency[item]}
                                            </Text>


                                        </View>

                                    }
                                    keyExtractor={item => item.id}
                                />



                                <FlatList
                                    data={this.state.resultDetails.technical_skills_metrics_overall_resume.ext_optional_skills_unique}
                                    renderItem={({ item, index }) =>

                                        <View style={{ width: WINDOW_WIDTH / 3, alignItems: 'center', margin: 5 }}>
                                            <Text style={{ color: 'grey' }}>
                                                {this.state.resultDetails.technical_skills_metrics_overall_resume.ext_optional_skills_mapped_frequency[item]}
                                            </Text>


                                        </View>

                                    }
                                    keyExtractor={item => item.id}
                                />



                            </View>

                        </View>
                    </View>

                    <View style={{ maxHeight: WINDOW_HEIGHT / 2.3, }}>

                        <Text style={styles.headerDesign}>Last Companies Exp Details</Text>

                        <FlatList

                            horizontal={true}
                            data={this.state.resultDetails.professional_summary_complete}
                            renderItem={({ item, index }) =>
                                <View style={styles.academicContainerDesign} >

                                    <Text style={styles.textDesign2}>--> {item.company_name}</Text>
                                    <Text style={styles.textDesign2}>--> {item.designation}</Text>
                                    <Text style={styles.textDesign2}>--> {item.duration.extracted_duration}</Text>
                                    <Text style={styles.textDesign2}>--> {item.duration.tenure_years} years</Text>

                                </View>


                            }
                            keyExtractor={item => item.id}
                        />

                        <View style={{ width: WINDOW_WIDTH, alignItems: 'flex-end' }}>
                            <Text style={{ color: '#1b75bb', margin: 5, fontSize: 10 }}>
                                Scroll Left-Right for more
</Text>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <View style={styles.cardShadow}>
                            <Text style={styles.headerDesign}>Contact Information</Text>

                            <View style={{ margin: 10, marginTop: 0 }}>
                                <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', }}>Contact No.</Text>
                                <Text style={{ color: 'grey', fontSize: 15 }}>(+91)-8888888888</Text>
                            </View>
                            <View style={{ margin: 10, marginTop: 0 }}>
                                <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', }}>E-mail Id</Text>
                                <Text style={{ color: 'grey', fontSize: 15, }}>{this.state.resultDetails.contact_email}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <View style={styles.cardShadow}>
                            <Text style={styles.headerDesign}>Online Presence</Text>
                            <View style={{ margin: 10, marginTop: 0 }}>
                                <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', }}>LinkedIn Profile</Text>
                                <Text style={{ color: '#1b75bb', fontSize: 15, textDecorationLine: 'underline' }} onPress={() => Linking.openURL(this.state.resultDetails.linkedin_url)}>{this.state.resultDetails.linkedin_url}</Text>
                            </View>
                            <View style={{ margin: 10, marginTop: 0 }}>
                                <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', }}>GitHub Profile</Text>
                                <Text style={{ color: '#1b75bb', fontSize: 15, textDecorationLine: 'underline' }} onPress={() => Linking.openURL(this.state.resultDetails.github_url)}>{this.state.resultDetails.github_url}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <View style={styles.cardShadow}>
                            <Text style={styles.headerDesign}>GitHub Profile Details</Text>

                            <Text style={{ color: '#fff', fontSize: 15, margin: 10 }}>{this.state.resultDetails.github_data}</Text>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center', marginBottom: 20 }}>
                        <View style={styles.cardShadow}>
                            <Text style={styles.headerDesign}>Supplementary Information</Text>

                            <View style={{ margin: 10, marginTop: 0 }}>
                                <Text style={{ color: 'grey', fontSize: 15, fontWeight: 'bold', }}>Start Date Last Job</Text>
                                <Text style={{ color: 'grey', fontSize: 15 }}>{this.state.resultDetails.professional_summary_complete[0].duration.start_date}</Text>
                            </View>
                        </View>
                    </View>
                </View>

            </ScrollView>
            <DropdownAlert ref={ref => this.dropDownAlertRef = ref} /></View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // alignItems: 'center',
        //justifyContent: 'center',
    },

    textDesign: {
        color: '#1b75bb',
        fontSize: 15,
        marginTop: 13,
        fontWeight: 'bold',

    },
    textDesign2: {
        color: 'grey',
        fontSize: 13,
        margin: 5,
        fontWeight: 'bold'
    },
    academicContainerDesign: {
        marginLeft: 10,
        marginRight: 5,
        // borderWidth: 1,
        padding: 5,
        borderColor: 'grey',
        backgroundColor: '#fff',
        borderRadius: 5,
        width: WINDOW_WIDTH / 2,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 10,
        borderWidth: .5,



    },
    headerDesign: {
        color: '#1b75bb',
        fontSize: 20,
        fontWeight: 'bold',
        margin: 10,

    },
    cardShadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 10,
        padding: 5,
        borderColor: 'grey',
        backgroundColor: '#fff',
        borderRadius: 5,
        width: WINDOW_WIDTH / 1.03,
        marginTop: 20,
        borderWidth: .5,
    }

});

export default ResultDetails;
