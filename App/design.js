
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    Image,
    TouchableOpacity,
    Platform
} from 'react-native';

export const font_family = StyleSheet.create({
    font:{
    fontFamily: 'Lato-Regular'
}
})