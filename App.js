/* eslint-disable prettier/prettier */
import React, { Component, StyleSheet,styles } from 'react';
import { Actions, Router, Scene, Stack, ActionConst } from 'react-native-router-flux';

import login from './App/login';
import SignUp from './App/signUp';
//import QrCode from './App/QrCode';
import splash from './App/splash';
import FormScreen from './App/formScreen';
import HomeScreen from './App/homeScreen';
import ResultScreen from './App/result';
import ResultDetails from './App/resultDetails';


/**
 * @Router :- Declaration of whole app screens for Navigation/routing.
 */


const App = () => {
  return (
    <Router    navigationBarStyle={{ backgroundColor:'#1b75bb',}} titleStyle={{  color:'#FFFFFF'}} barButtonTextStyle={{ color:'#FFFFFF'}}  >


      <Scene key="root">





        <Scene key="splash"
          component={splash}
          hideNavBar={true}
         initial
        />
        <Scene key="login"
          component={login}
          hideNavBar={true}

        />
         <Scene key="SignUp"
          component={SignUp}
          hideNavBar={false}
          headerTintColor="#fff"

        />
         <Scene key="FormScreen"
          component={FormScreen}
          hideNavBar={false}
          headerTintColor="#fff"

        />
         <Scene key="ResultScreen"
          component={ResultScreen}
          hideNavBar={false}
          headerTintColor="#fff"
          //initial
        />
         <Scene key="HomeScreen"
          component={HomeScreen}
          headerTintColor="#fff"

          //initial
        />
        <Scene key="ResultDetails"
          component={ResultDetails}
          hideNavBar={false}
          headerTintColor="#fff"
          //initial
        />
        {/* <Scene
          key="QrCode"
          component={QrCode}
          title="Scan"
          navigationBarStyle={{ backgroundColor: '#6EC4D1' }}
          titleStyle={{ color: "#fff" }}
          tintColor='#fff'
          hideNavBar={true}
        /> */}
      </Scene>

    </Router>
    
  );
  
};


export default App;
